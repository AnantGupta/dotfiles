# My .bashrc file
# Contains all the customization regarding the bash shell

# Customized prompt 
PS1='\W λ '

# Vi mode
set -o vi
bind -m vi-command 'Control-l: clear-screen'
bind -m vi-insert 'Control-l: clear-screen'

# Exports
export PATH=$PATH:/home/anant/.local/bin
export MANPAGER="sh -c 'col -bx | bat -l man -p'"
export EDITOR="nvim"
export BROWSER="firefox"
export HISTCONTROL=ignoreboth

# Aliases
alias config="/usr/bin/git --git-dir=$HOME/dotfiles/ --work-tree=$HOME"
alias configurl="config remote set-url origin"
alias giturl="git remote set-url origin"
alias gp="git push origin master"
alias gs="git status"
alias install="yay -S --noconfirm --needed"
alias less="bat"
alias ls='exa'
alias remove="yay -Rs --noconfirm"
alias sudo='doas'
alias update="yay -Syyu --devel --noconfirm"
alias v="nvim"
