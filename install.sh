#!/bin/bash

# My install script for an arch based system. This will deploy a machine with the 'dwm' window manager
# 'alacritty' terminal emulator etc. Also it will overwrite the default config ie removing .config/ folder and every other folder you have
# so use this script & your own risk :)

echo "Installing base-devel"
sudo pacman -S --needed --noconfirm base-devel

echo "This will install for a very minimal distro like Arch or Artix Base install, this will overwrite your .config & other files & folder, which will remove all the configs which you might have made. Also it will remove your personal folders. Do a ctrl+c if you want to force quit & if you want manuall stuff & clone whatever & overwrite you want"
echo "PS: This will sleep for 10 seconds so don't think that the program is not working"
sleep 10 

echo "Installing yay AUR helper"
cd
git clone https://aur.archlinux.org/yay-git.git 
cd yay-git/
makepkg -si

echo "Installing my requiring packages, that is essential for the machine"
PKGS=(
    'alacritty' # Terminal Emulator
    'alsa-utils' # For sound
    'bat' # Less replacement
    'doas' # sudo replacement
    'dunst' # Notification
    'exa' # ls alternative
    'feh' # For wallpaper
    'flameshot' # screenshot
    'git' # version control system
    'libnotify' # Notification daemon
    'light-git' # For brightness
    'mpv' # Video player
    'neovim' # Text editor, vim alternative
    'networkmanager' # For network
    'sxhkd' # For keybinding
    'sxiv' # Image viewer
    'ttf-fantasque-sans-mono' # Font
    'xorg-server' # Xorg server
    'xorg-xinit' # Start xorg
    'xorg-xmonadmap' # For mapping one key to another
    'xorg-xrdb' # For xresources
    'xsel' # Clipboard for vim
)

# Creating a loop for installing the packages
for PKG in "${PKGS[@]}"; do
  doas yay -S "$PKG" --noconfirm --needed
done

echo "Now moving configs"
cd ~/
shopt -s extglob
rm -rf !(.bash_profile|dotfiles) *
mkdir Downloads
mkdir suckless
cd ~/dotfiles
cp -r * ~/
cp * ~/

echo "### Installing Suckless Software ###" 

echo "Installing dwm"
cd ~/suckless
git clone https://gitlab.com/AnantGupta/dwm.git
cd dwm
sudo make clean install && rm -rf *o

echo "Now installing dmenu"
cd ~/suckless
git clone https://gitlab.com/AnantGupta/dmenu.git
cd dmenu
sudo make clean install && rm -rf *o

read -p "Final question. Are you Anant?(y/n) (Please tell the truth or you have to enter the password): " anant

if [ $anant == 'y' ]
then
    echo "### Installing packages ###"

    echo "Now installing other packages"
    PKGS=(
        'arc-gtk-theme' # GTK theme
        'code' # VS Code
        'firefox' # Browser
        'htop' # System Resource Checker
        'libreoffice-fresh' # Office Suit
        'lxappearance' # For setting gtk theme
        'man-db' # For reading man pages
        'npm' # Node package manager
        'obs-studio' # For recording videos
        'pavucontrol' # For setting audio
        'pulseaudio' # For audio
        'ttf-indic-otf' # Indian fonts
        'unzip' # For unzipping
        'xdg-utils' # For managing XDG mime apps
        'xf86-video-intel' # Intel drivers
    )

    # Creating a loop for installing the packages
    for PKG in "${PKGS[@]}"; do
      doas yay -S "$PKG" --noconfirm --needed
    done

    echo "Cloning personal folder"
    mkdir ~/git-dir
    cd ~/git-dir
    git clone https://gitlab.com/AnantGupta/personal.git
    git clone https://gitlab.com/AnantGupta/calculator.git
    git clone https://gitlab.com/AnantGupta/chatbot-for-atl-2.git
    git clone https://gitlab.com/AnantGupta/chatbot-web.git
    git clone https://gitlab.com/AnantGupta/cricket-scoreboard.git
    git clone https://gitlab.com/AnantGupta/cricket-stats.git
    git clone https://gitlab.com/AnantGupta/minfetch.git
    git clone https://gitlab.com/AnantGupta/my-website.git
    git clone https://gitlab.com/AnantGupta/non-profit.git
    git clone https://gitlab.com/AnantGupta/school-talent-searchers.git
    git clone https://gitlab.com/AnantGupta/terminal-help.git

    cd minfetch/
    sudo cp minfetch /bin/
    git config --global user.name "Anant Gupta"
    git config --global user.email "gupta.anant.1303@gmail.com"
    xdg-mime default sxiv.desktop application/png
    xdg-mime default sxiv.desktop application/jpeg
    echo "Done, bye"
elif [ $anant == 'n' ]
then
    echo "Done, bye"
else
    echo "Wrong lol"
fi
