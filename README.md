# Dotfiles

My typical Dotfiles for the programs I use regularly & have done customization on that.
## Notes
These are some programs I use, you can find why it is useful, description & where you can find my config of that program

| Programs  | Why it is useful     | Description                |
| :-------- | :------- | :------------------------- |
| `dwm` | A Window Manager | DWM is the original dynamic tiling window manager, which is written in `c`. I am using it as my main tiling window manager. You can find this at my dwm repo at https://gitlab.com/AnantGupta/dwm |
| `alacritty` | Terminal emulator | alacritty terminal is a terminal written in rust. It claims it to be the fastest terminal emulator. Find my config at https://gitlab.com/AnantGupta/dotfiles/-/blob/master/.config/alacritty/alacritty.yml
| `bash` | A shell | bash is a `shell`, can be used as a replacement for something like bash. It provides with me something like vi mode etc, you can get my config at https://gitlab.com/AnantGupta/dotfiles/-/blob/master/.bashrc |
| `neovim` | A text editor | Neovim is a fork of standark `vim text editor`. I just prefer it over standard vim cause of system clipboard support. I am not able to add it in vim. Find my config at https://gitlab.com/AnantGupta/dotfiles/-/tree/master/.config/nvim |
## A screenshot...

![screenshot](https://i.imgur.com/wh2ofV0.png)

## 🚀 About Me
I'm Anant Gupta... I have done 99% of the work you are seeing here (1% may be copied from somewhere). I hope you liked the so-called rice :p
